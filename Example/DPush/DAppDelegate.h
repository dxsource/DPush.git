//
//  DAppDelegate.h
//  DPush
//
//  Created by douglas525264 on 12/09/2018.
//  Copyright (c) 2018 douglas525264. All rights reserved.
//

@import UIKit;

@interface DAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
