//
//  main.m
//  DPush
//
//  Created by douglas525264 on 12/09/2018.
//  Copyright (c) 2018 douglas525264. All rights reserved.
//

@import UIKit;
#import "DAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DAppDelegate class]));
    }
}
