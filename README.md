# DPush

[![CI Status](https://img.shields.io/travis/douglas525264/DPush.svg?style=flat)](https://travis-ci.org/douglas525264/DPush)
[![Version](https://img.shields.io/cocoapods/v/DPush.svg?style=flat)](https://cocoapods.org/pods/DPush)
[![License](https://img.shields.io/cocoapods/l/DPush.svg?style=flat)](https://cocoapods.org/pods/DPush)
[![Platform](https://img.shields.io/cocoapods/p/DPush.svg?style=flat)](https://cocoapods.org/pods/DPush)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

DPush is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'DPush'
```

## Author

douglas525264, 17601621681@163.com

## License

DPush is available under the MIT license. See the LICENSE file for more info.
