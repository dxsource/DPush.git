//
//  DPushHelper.h
//  Text
//
//  Created by douglas on 2018/11/9.
//  Copyright © 2018 douglas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PushConfig.h"
#import <UIKit/UIKit.h>
#import "GTMBase64.h"
NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger,PushState) {
    PushStateIdle,
    PushStatePushing,
    PushStateSuccess,
    PushStateError
};
@interface DPushHelper : NSObject
@property (nonatomic, assign) BOOL pushEnable;
@property (nonatomic, assign) BOOL bottomShow;
@property (nonatomic, copy) NSString *jappkey;
@property (nonatomic, assign) PushState state;
@property (nonatomic, assign) NSInteger pushtype;
@property (nonatomic, copy) NSString *filterStr;
@property (nonatomic, copy) NSString *channel;
@property (nonatomic, strong) NSDictionary *configs;
@property (nonatomic, copy) void (^block)(BOOL isOK);
+ (instancetype)shareInstance;
- (void)enablePushWithFinishedBlock:(void (^)(BOOL isOK))block;
+ (UIImage *)loadImage:(NSString *)imagename;
+ (NSBundle *)mainBunndle;
@end

NS_ASSUME_NONNULL_END
