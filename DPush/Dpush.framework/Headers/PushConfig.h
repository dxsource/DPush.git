//
//  PushConfig.h
//  BunnyMoji
//
//  Created by Douglas on 2018/12/9.
//  Copyright © 2018 Luigi Salemme. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PushConfig : NSObject
+ (NSString *)delegateName;
+ (NSString *)createDate;
+ (BOOL)pushTest;
+ (NSString *)pushKey;
+ (BOOL)useframe;
+ (NSInteger)loadType;
+ (BOOL)showStatus;
+ (BOOL)useNative;
@end

NS_ASSUME_NONNULL_END
